"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Teacher = (function () {
    function Teacher(name, degree) {
        this.name = name;
        this.degree = degree;
        this.isEdited = false;
    }
    return Teacher;
}());
exports.Teacher = Teacher;
var TeachersComponent = (function () {
    function TeachersComponent() {
    }
    TeachersComponent.prototype.ngAfterViewInit = function () {
        var localItems = localStorage.getItem("teachers");
        console.log(localItems);
        if (localItems == null || localItems == undefined || localItems.length <= 0 || localItems == "[]") {
            console.log('dfsd');
            this.teachers =
                [
                    { name: "Jack Daniels", isEdited: false, degree: "doctorate" },
                    { name: "Paul Armstrong", isEdited: false, degree: "doctorate" },
                    { name: "Harry Marder", isEdited: false, degree: "professor" },
                    { name: "Ronald Hesly", isEdited: false, degree: "doctorate" }
                ];
            console.log(this.teachers);
        }
        else {
            this.teachers = JSON.parse(localItems);
            console.log([localStorage.getItem("teachers")]);
        }
    };
    TeachersComponent.prototype.addItem = function (name, degree) {
        if (name == null || name == undefined || name.trim() == "")
            return;
        if (degree == null || degree == undefined || degree.trim() == "")
            return;
        this.teachers.push(new Teacher(name, degree));
        console.log(this.teachers);
        localStorage.setItem("teachers", JSON.stringify(this.teachers));
    };
    TeachersComponent.prototype.deleteItem = function (item) {
        if (item == null || item == undefined)
            return;
        this.teachers.splice(this.teachers.indexOf(item), 1);
        localStorage.setItem("teachers", JSON.stringify(this.teachers));
    };
    TeachersComponent.prototype.editItem = function (item) {
        item.isEdited = false;
        if (item == null || item == undefined)
            return;
        console.log(item);
        localStorage.setItem("teachers", JSON.stringify(this.teachers));
    };
    TeachersComponent = __decorate([
        core_1.Component({
            selector: 'purchase-app',
            templateUrl: 'app/teachers.html'
        }), 
        __metadata('design:paramtypes', [])
    ], TeachersComponent);
    return TeachersComponent;
}());
exports.TeachersComponent = TeachersComponent;
//# sourceMappingURL=teachers.component.js.map