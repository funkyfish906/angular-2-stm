import { Component } from '@angular/core';
import { LocalStorage, SessionStorage } from '@angular/localstorage';

export class Student{
    name: string;
    group: number;
    isEdited: boolean;
     
    constructor(name: string, group: number) {
  
        this.name = name;
        this.group = group;
        this.isEdited = false;  
    }
}


@Component({
    selector: 'purchase-app',
    templateUrl: 'app/students.html'
})
export class StudentsComponent{ 

    students: Student[];

    ngAfterViewInit(){

        let localItems = localStorage.getItem("students");
        
        if(localItems==null || localItems==undefined || localItems.length<=0 || localItems=="[]"){
            this.students =
            [
                { name: "Jack Daniels", isEdited: false, group: 101 },
                { name: "Paul Armstrong", isEdited: false, group: 101 },
                { name: "Harry Marder", isEdited: false, group: 102 },
                { name: "Ronald Hesly", isEdited: false, group: 103 }
            ];
        }
        else{
           this.students = JSON.parse(localItems);
           console.log([localStorage.getItem("students")]);
            
        }
    }

    addItem(name: string, group: number): void {
         
        if(name==null || name==undefined || name.trim()=="")
            return;
        if(group==null || group==undefined)
            return;
        this.students.push(new Student(name, group));
        console.log(this.students);
        localStorage.setItem("students", JSON.stringify(this.students));

    }

    deleteItem(item): void {

        if(item==null || item==undefined)
            return;
        this.students.splice(this.students.indexOf(item), 1);
        localStorage.setItem("students", JSON.stringify(this.students));
    }



    editItem(item): void {
        item.isEdited = false;

        if(item==null || item==undefined)
            return;
        console.log(item);
        localStorage.setItem("students", JSON.stringify(this.students));
    }
  
}