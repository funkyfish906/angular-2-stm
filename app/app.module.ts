import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { AppComponent }   from './app.component';
import { StudentsComponent }   from './students.component';
import { TeachersComponent }   from './teachers.component';
import {Routes, RouterModule} from '@angular/router';
const appRoutes: Routes =[
    { path: '', component: AppComponent},
    { path: 'students', component: StudentsComponent},
    { path: 'teachers', component: TeachersComponent},
    { path: '**', redirectTo: '/'}
];
@NgModule({
    imports:      [ BrowserModule, FormsModule, RouterModule.forRoot(appRoutes)],
    declarations: [ AppComponent, StudentsComponent, TeachersComponent],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }